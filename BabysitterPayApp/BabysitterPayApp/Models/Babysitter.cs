﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BabysitterPayApp.Models
{
    public class Babysitter
    {
        public int ConvertHours(int hour)
        {
            int convertedHour = hour;

            if(hour >= 1 && hour <= 4)
            {
                convertedHour += 12;
            }
            return convertedHour;
        }
    }
}
