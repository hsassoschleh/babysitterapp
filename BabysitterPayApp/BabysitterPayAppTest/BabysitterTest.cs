﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BabysitterPayApp.Models;


namespace BabysitterPayAppTest
{
    [TestClass]
    public class BabysitterTest
    {
        [TestMethod]
        public void whenConvertHoursPassedHourBeginsReturnsThatHour()
        {
            Babysitter babysitter = new Babysitter();
            int result = babysitter.ConvertHours(5);
            Assert.AreEqual(5, result);

        }

        [TestMethod]
        public void whenConvertHoursPassed4BeginsReturns16()
        {
            Babysitter babysitter = new Babysitter();
            int result = babysitter.ConvertHours(4);
            Assert.AreEqual(16, result);

        }


    }
}

